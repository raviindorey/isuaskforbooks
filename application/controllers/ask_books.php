<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ask_books extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		$this -> load -> model('reserve_book');
		$this -> load -> helper('my_helper');

		if ($this -> session -> userdata('logged_in')) {
			$session_data = $this -> session -> userdata('logged_in');

			$student_email = $session_data['user_email'];
			$start_date = $this->input->post('req_datetime_from');
			$return_date = $this -> input -> post('req_datetime_till');
			$return_date_timestamp = strtotime($return_date);
			$start_date_timestamp = strtotime($start_date);
			$req_sess_data = $this -> session -> userdata('req_data');

			$book_id = $req_sess_data['book_id'];
			$faculty_email = $req_sess_data['faculty_email'];
			$data['user_full_name'] = $session_data['user_full_name'];
		

			$this -> form_validation -> set_rules('req_datetime_from', 'Start Date', 'trim|required');
			$this -> form_validation -> set_rules('req_datetime_till', 'End Date', 'trim|required');

			if ($this -> form_validation -> run() == FALSE) {
				redirect("find_books");
			} else {
				//save request
				$query = $this -> reserve_book -> setBookRequest($book_id, $student_email, $faculty_email, $start_date_timestamp, $return_date_timestamp);
				//redirect
				redirect("find_books");
			}

		} else {
			//If no session, redirect to login page
			redirect('home', 'refresh');
		}

	}

}
?>