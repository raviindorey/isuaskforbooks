<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class book_requests extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		$this -> load -> model('reserve_book');
		$this -> load -> helper('my_helper');

		$session_data = $this -> session -> userdata('logged_in');
		$user_email = $session_data['user_email'];

		//get all requests from database
		$user_email = $session_data['user_email'];
		$all_req_query = $this -> reserve_book -> getAllRequests($user_email, $session_data['user_account_type']);
		$book_req_data['request_array'] = $all_req_query;
		
		$book_req_data['user_email'] = $session_data['user_email'];
		$book_req_data['user_full_name'] = $session_data['user_full_name'];

		if ($session_data['user_account_type'] == '1') {
			$this -> load -> view('book_request_faculty_view', $book_req_data);
		} else {
			$this -> load -> view('book_request_student_view', $book_req_data);
		}

	}

}
?>