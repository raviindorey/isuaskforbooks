<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class book_requests_approve extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		if ($this -> session -> userdata('logged_in')) {

			$this -> load -> model('reserve_book');
			$this -> load -> helper('my_helper');

			$session_data = $this -> session -> userdata('logged_in');
			$user_email = $session_data['user_email'];

		} else {
			redirect('home', 'refresh');
		}
	}

	function approveBookRequests($request_id) {

		$this -> load -> model('reserve_book');
		$this -> load -> helper('my_helper');
		
		$session_data = $this -> session -> userdata('logged_in');
			$user_email = $session_data['user_email'];

		//get all request details from database
		$query = $this -> reserve_book -> getRequestDetail($request_id);
		foreach ($query->result() as $row) {
			$book_id = $row -> book_id;
			$start_date = $row -> start_date;
			$return_date = $row -> return_date;
			$student_email = $row -> student_email;
		}

		//get book title
		$get_book_name_query = $this -> reserve_book -> getFaculty_from_faculty_books($book_id);
		foreach ($get_book_name_query->result() as $row) {
			$book_title = $row -> book_title;
		}

		//approve requests setting reservation
		$this -> reserve_book -> approveBookrequest($book_id, $book_title, $start_date, $return_date, $student_email);
		
		//delete the request 
		$this -> reserve_book ->deleteApprovedRequest($request_id);
		
		//add to history
		$this -> reserve_book ->insertHistory($request_id, $student_email, $user_email, $start_date, $return_date, $book_title);

		//redirect
		redirect('book_requests', 'refresh');
	}

}
?>