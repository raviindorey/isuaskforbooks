<?php

class editprofile_faculty extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		$this -> load -> model('faculty_information');

		$this -> session -> userdata('logged_in');
		$session_data = $this -> session -> userdata('logged_in');

		if ($session_data['user_account_type'] == '1') {

			$data['user_email'] = $session_data['user_email'];
			$data['user_full_name'] = $session_data['user_full_name'];

			$if_already_present = $this -> db -> query("SELECT * FROM `faculty_information` WHERE `faculty_email` = '" . $data['user_email'] . "'");

			if ($if_already_present -> num_rows() > 0) {
				foreach ($if_already_present->result() as $row) {
					$data['faculty_about'] = $row -> faculty_about;
					$data['faculty_subjects'] = $row -> faculty_classes;
					$data['faculty_office_hours'] = $row -> faculty_office_hours;
					$data['faculty_office_location'] = $row -> faculty_office_location;
					$data['faculty_contact_email2'] = $row -> faculty_email_2;
					$data['faculty_contact_phone1'] = $row -> faculty_phone_1;
					$data['button_value'] = "Update Profile";
				}
			} else {
				$data['faculty_about'] = "";
				$data['faculty_subjects'] = "";
				$data['faculty_office_hours'] = "";
				$data['faculty_office_location'] = "";
				$data['faculty_contact_email2'] = "";
				$data['faculty_contact_phone1'] = "";
				$data['button_value'] = "Create Profile";
			}

			$this -> form_validation -> set_rules('about', 'About', 'trim');
			$this -> form_validation -> set_rules('subjects', 'Subjects', 'trim|required');
			$this -> form_validation -> set_rules('office_hours', 'Office Hours', 'trim|required');
			$this -> form_validation -> set_rules('office_location', 'Office Location', 'trim|required');
			$this -> form_validation -> set_rules('contact_email2', 'Other Email', 'valid_email|trim');
			$this -> form_validation -> set_rules('contact_phone1', 'Phone Number', 'trim');

			if ($this -> form_validation -> run() == FALSE) {
				$this -> load -> view('editprofile_faculty_view', $data);
			} else {
				//assign
				$email = $session_data['user_email'];
				$about = $this -> input -> post('about');
				$subjects = $this -> input -> post('subjects');
				$office_hours = $this -> input -> post('office_hours');
				$office_location = $this -> input -> post('office_location');
				$contact_email2 = $this -> input -> post('contact_email2');
				$contact_phone1 = $this -> input -> post('contact_phone1');

				//Save to database
				if ($if_already_present -> num_rows() > 0) {
					$this -> faculty_information -> updateFacultyInformation($email, $about, $subjects, $office_hours, $office_location, $contact_email2, $contact_phone1);
				} else {
					$this -> faculty_information -> createFacultyInformation($email, $about, $subjects, $office_hours, $office_location, $contact_email2, $contact_phone1);
				}

				//redirect
				redirect('viewmybooks_faculty', $data);
			}

		} else {
			//If no session, redirect to login page
			redirect('home', 'refresh');
		}

	}

}
?>