<?php

class editprofile_student extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		$this -> load -> model('student_information');

		$this -> session -> userdata('logged_in');
		$session_data = $this -> session -> userdata('logged_in');

		if ($session_data['user_account_type'] == '0') {

			$data['user_email'] = $session_data['user_email'];
			$data['user_full_name'] = $session_data['user_full_name'];

			$if_already_present = $this -> db -> query("SELECT * FROM `student_information` WHERE `student_email` = '" . $data['user_email'] . "'");

			if ($if_already_present -> num_rows() > 0) {
				foreach ($if_already_present->result() as $row) {
					$data['student_about'] = $row -> student_about;
					$data['student_subjects'] = $row -> student_classes;
					$data['student_contact_email2'] = $row -> student_email_2;
					$data['student_contact_phone1'] = $row -> student_phone_1;
					$data['button_value'] = "Update Profile";
				}
			} else {
				$data['student_about'] = "";
				$data['student_subjects'] = "";
				$data['student_contact_email2'] = "";
				$data['student_contact_phone1'] = "";
				$data['button_value'] = "Create Profile";
			}

			$this -> form_validation -> set_rules('about', 'About', 'trim');
			$this -> form_validation -> set_rules('subjects', 'Subjects', 'trim|required');
			$this -> form_validation -> set_rules('contact_email2', 'Other Email', 'valid_email|trim');
			$this -> form_validation -> set_rules('contact_phone1', 'Phone Number', 'trim');

			if ($this -> form_validation -> run() == FALSE) {
				$this -> load -> view('editprofile_student_view', $data);
			} else {
				//assign
				$email = $session_data['user_email'];
				$about = $this -> input -> post('about');
				$subjects = $this -> input -> post('subjects');
				$contact_email2 = $this -> input -> post('contact_email2');
				$contact_phone1 = $this -> input -> post('contact_phone1');

				//Save to database
				if ($if_already_present -> num_rows() > 0) {
					$this -> student_information -> updateStudentInformation($email, $about, $subjects, $contact_email2, $contact_phone1);
				} else {
					$this -> student_information -> createStudentInformation($email, $about, $subjects, $contact_email2, $contact_phone1);
				}

				//redirect
				redirect('viewmybooks_student', $data);
			}

		} else {
			//If no session, redirect to login page
			redirect('home', 'refresh');
		}

	}

}
?>