<?php

class faculty_managebooks_controller extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		$this -> load -> model(array('faculty_books', 'book_reservation'));
		if ($this -> session -> userdata('logged_in')) {
			$session_data = $this -> session -> userdata('logged_in');
		} else {
			//If no session, redirect to login page
			redirect('home', 'refresh');
		}

	}

	function delete_book($book_id) {
		if ($this -> session -> userdata('logged_in')) {
			$session_data = $this -> session -> userdata('logged_in');
			if ($session_data['user_account_type'] == '1') {

				$tables = array('faculty_books', 'book_reservation');
				$this -> db -> query("DELETE FROM `faculty_books` WHERE `book_id` = '" . $book_id . "'");
				$this -> db -> query("DELETE FROM `book_reservation` WHERE `book_id` = '" . $book_id . "'");

				redirect('viewmybooks_faculty', 'refresh');

			}
		}
	}

}
?>