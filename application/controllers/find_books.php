<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class find_books extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		$this -> load -> model('reserve_book');
		$this -> load -> helper('my_helper');

		if ($this -> session -> userdata('logged_in')) {
			$session_data = $this -> session -> userdata('logged_in');

			$data['user_email'] = $session_data['user_email'];
			$data['user_full_name'] = $session_data['user_full_name'];
			$data['viewmybooks'] = ($session_data['user_account_type'] == '1') ? "viewmybooks_faculty" : "viewmybooks_student";
			$data['editprofile'] = ($session_data['user_account_type'] == '1') ? "editprofile_faculty" : "editprofile_student";

			$data['faculty_all_books'] = $this -> reserve_book -> getAllBooks_from_faculty_books();
			
			$this -> load -> view('find_books_view', $data);

		} else {
			//If no session, redirect to login page
			redirect('home', 'refresh');
		}

	}

	function redirectToRequest($book_id) {
		$session_data = $this -> session -> userdata('logged_in');
		$this -> load -> model('reserve_book');
		$this -> load -> helper('my_helper');
		
		$req_data['user_full_name'] = $session_data['user_full_name'];
		$req_data['user_email'] = $session_data['user_email'];

		//getting faculty email and book title
		$fac_query_email = $this -> reserve_book -> getFaculty_from_faculty_books($book_id);

		foreach ($fac_query_email->result() as $fac_query_res) {
			$req_data['faculty_email'] = $fac_query_res -> faculty_email;
			$req_data['book_title'] = $fac_query_res -> book_title;

		}

		//getting faculty name
		$fac_query_full_name = $this -> reserve_book -> getName_from_user_table($req_data['faculty_email']);

		foreach ($fac_query_full_name->result() as $fac_query_full_name_res) {
			$req_data['faculty_full_name'] = $fac_query_full_name_res -> user_full_name;
		}

		//getting rest of faculty information
		$fac_query_details = $this -> reserve_book -> getFacultyInfo_from_faculty_information($req_data['faculty_email']);
		if ($fac_query_details -> num_rows() > 0) {
			foreach ($fac_query_details->result() as $fac_query_details_res) {
				if ($fac_query_details_res -> faculty_about != null) {
					$req_data['faculty_about'] = $fac_query_details_res -> faculty_about;
				} else {
					$req_data['faculty_about'] = "N/A";
				}
				if ($fac_query_details_res -> faculty_profile_pic != null) {
					$req_data['faculty_profile_pic'] = $fac_query_details_res -> faculty_profile_pic;
				} else {
					$req_data['faculty_profile_pic'] = "N/A";
				}
				$req_data['faculty_classes'] = $fac_query_details_res -> faculty_classes;
				$req_data['faculty_office_hours'] = $fac_query_details_res -> faculty_office_hours;
				$req_data['faculty_office_location'] = $fac_query_details_res -> faculty_office_location;
				if ($fac_query_details_res -> faculty_email_2 != null) {
					$req_data['faculty_email_2'] = $fac_query_details_res -> faculty_email_2;
				} else {
					$req_data['faculty_email_2'] = "N/A";
				}
				if ($fac_query_details_res -> faculty_phone_1 != null) {
					$req_data['faculty_phone_1'] = $fac_query_details_res -> faculty_phone_1;
				} else {
					$req_data['faculty_phone_1'] = "N/A";
				}

				$req_data['student_email'] = $session_data['user_email'];
				$req_data['book_id'] = $book_id;

			}
		} else {
			$req_data['faculty_about'] = "N/A";
			$req_data['faculty_classes'] = "N/A";
			$req_data['faculty_office_hours'] = "N/A";
			$req_data['faculty_office_location'] = "N/A";
			$req_data['faculty_email_2'] = "N/A";
			$req_data['faculty_phone_1'] = "N/A";
			$req_data['student_email'] = "N/A";
			$req_data['book_id'] = "N/A";
		}

		//getting book reservations
		$book_query_all_reservations = $this -> reserve_book -> getBookReservation_from_book_reservation($book_id);
		if ($book_query_all_reservations -> num_rows() > 0) {
			foreach ($book_query_all_reservations->result() as $book_query_all_reservations_res) {
				$reserve_end = $book_query_all_reservations_res -> reserve_end;
				$req_data['next_available'] = timestamp_to_date($reserve_end);
			}
		} else {
			$req_data['next_available'] = "Available Now";
		}
		$req_sess = array('book_id' => $book_id, 'faculty_email' => $req_data['faculty_email']);
		$this -> session -> set_userdata('req_data', $req_sess);
		$this -> load -> view('request_view', $req_data);
	}

}
?>