<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		if (!($this -> db -> table_exists('faculty_books'))) {
			$this -> db -> query("CREATE TABLE `isuaskforbooks`.`faculty_books` ( `book_id` int NOT NULL AUTO_INCREMENT, `faculty_email` VARCHAR(30) NOT NULL , `book_title` VARCHAR(50) NOT NULL , `book_description` TEXT NOT NULL , `book_isbn` VARCHAR(20) NOT NULL , `book_subject_code` VARCHAR(50) NOT NULL , `book_publication_year` INT(5) NOT NULL, PRIMARY KEY (`book_id`) ) ENGINE = InnoDB");
		}
		if (!($this -> db -> table_exists('faculty_information'))) {
			$this -> db -> query("CREATE TABLE `isuaskforbooks`.`faculty_information` ( `faculty_email` VARCHAR(30) NOT NULL , `faculty_profile_pic` MEDIUMBLOB NULL , `faculty_about` TEXT NULL , `faculty_classes` VARCHAR(30) NOT NULL , `faculty_office_hours` VARCHAR(40) NOT NULL , `faculty_office_location` VARCHAR(50) NOT NULL , `faculty_email_2` VARCHAR(30) NULL , `faculty_phone_1` VARCHAR(15) NULL ) ENGINE = InnoDB");
		}
		if (!($this -> db -> table_exists('book_history'))) {
			$this -> db -> query("CREATE TABLE `isuaskforbooks`.`book_history` ( `reserve_id` INT NOT NULL AUTO_INCREMENT, `reserve_to` VARCHAR(30) NOT NULL , `reserve_from` VARCHAR(30) NOT NULL , `reserve_date_start` VARCHAR(30) NOT NULL , `reserve_date_end` VARCHAR(30) NOT NULL , `book_title` VARCHAR(50) NOT NULL, PRIMARY KEY (`reserve_id`)) ENGINE = InnoDB");
		}
		if (!($this -> db -> table_exists('book_request'))) {
			$this -> db -> query("CREATE TABLE `isuaskforbooks`.`book_request` ( `request_id` INT NOT NULL AUTO_INCREMENT, `book_id` INT(30) NOT NULL , `student_email` VARCHAR(30) NOT NULL , `faculty_email` VARCHAR(30) NOT NULL , `start_date` VARCHAR(30) NOT NULL , `return_date` VARCHAR(30) NOT NULL,  PRIMARY KEY (`request_id`)) ENGINE = InnoDB");
		}
		if (!($this -> db -> table_exists('book_reservation'))) {
			$this -> db -> query("CREATE TABLE `isuaskforbooks`.`book_reservation` ( `book_id` INT(30) NOT NULL , `book_title` VARCHAR(50) NOT NULL , `reserve_start` VARCHAR(30) NOT NULL , `reserve_end` VARCHAR(30) NOT NULL , `given_to` VARCHAR(30) NOT NULL) ENGINE = InnoDB");
		}
		if (!($this -> db -> table_exists('user_table'))) {
			$this -> db -> query("CREATE TABLE `isuaskforbooks`.`user_table`( `user_email` VARCHAR(30) NOT NULL , `user_title` VARCHAR(10) NULL , `user_full_name` VARCHAR(40) NOT NULL , `user_password` VARCHAR(40) NOT NULL , `user_account_type` INT(1) NOT NULL , PRIMARY KEY (`user_email`) ) ENGINE = InnoDB;");
		}
		if (!($this -> db -> table_exists('student_information'))) {
			$this -> db -> query("CREATE TABLE `isuaskforbooks`.`student_information` ( `student_email` VARCHAR(30) NOT NULL , `student_profile_pic` MEDIUMBLOB NULL , `student_about` TEXT NULL , `student_classes` VARCHAR(50) NOT NULL , `student_email_2` VARCHAR(30) NULL , `student_phone_1` INT(15) NULL , PRIMARY KEY (`student_email`(30))) ENGINE = InnoDB;");
		}
	}

	function index() {
		$this -> load -> view('home');
		$this -> load -> dbutil();
		$this -> load -> dbforge();

		if (!($this -> dbutil -> database_exists('isuaskforbooks'))) {
			$this -> dbforge -> create_database('isuaskforbooks');
		}

		$this -> session -> sess_destroy();
	}

}
?>