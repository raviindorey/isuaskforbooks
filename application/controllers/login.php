<?php

class Login extends CI_Controller {

	function index() {
		$this -> load -> model('login_model');

		$this -> form_validation -> set_rules('email_login', 'Email Address', 'valid_email|trim|required');
		$this -> form_validation -> set_rules('password_login', 'Password', 'trim|required|callback_check_credentials');

		if ($this -> form_validation -> run() == FALSE) {
			$this -> load -> view('home');
		} else {
			$session_data = $this -> session -> userdata('logged_in');

			$data['user_email'] = $session_data['user_email'];
			$data['user_title'] = $session_data['user_title'];
			$data['user_full_name'] = $session_data['user_full_name'];
			$data['user_account_type'] = $session_data['user_account_type'];

			if ($session_data['user_account_type'] == '0') {
				redirect('find_books', $data);
			} elseif ($session_data['user_account_type'] == '1') {
				redirect('viewmybooks_faculty', $data);
			}

		}
	}

	function check_credentials($password) {
		$email = $this -> input -> post('email_login');
		$result = $this -> login_model -> login_check($email, sha1($password));
		if ($result) {
			return true;
		} else {
			$this -> form_validation -> set_message('check_credentials', 'Wrong username or password!');
			return false;
		}
	}

}
