<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class search extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		$this -> load -> model('reserve_book');
		$this -> load -> helper('my_helper');

		if ($this -> session -> userdata('logged_in')) {
			$session_data = $this -> session -> userdata('logged_in');

			$data['user_email'] = $session_data['user_email'];
			$data['user_full_name'] = $session_data['user_full_name'];
			$data['viewmybooks'] = ($session_data['user_account_type'] == '1') ? "viewmybooks_faculty" : "viewmybooks_student";
			$data['editprofile'] = ($session_data['user_account_type'] == '1') ? "editprofile_faculty" : "editprofile_student";
			$data['faculty_email'] = $this->input->post('search_text');
			
			$search_keyword = $this->input->post('search_text');

			$data['faculty_search_books'] = $this -> reserve_book -> getSearchBooks_from_faculty_books($search_keyword);
			
			$this -> load -> view('search_view', $data);

		} else {
			//If no session, redirect to login page
			redirect('home', 'refresh');
		}

	}

}
?>