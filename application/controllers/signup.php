<?php

class Signup extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {
		$this -> load -> model('signup_model');

		$this -> form_validation -> set_rules('title_signup', 'Title', 'trim');
		$this -> form_validation -> set_rules('full_name', 'Name', 'trim|required');
		$this -> form_validation -> set_rules('email', 'Email Address', 'trim|required|valid_email|callback_check_database_for_email');
		$this -> form_validation -> set_rules('email_confirm', 'Confirm Email', 'trim|required|matches[email]');
		$this -> form_validation -> set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this -> form_validation -> set_rules('password_confirmation', 'Password Confirmation', 'trim|required|matches[password]');
		$this -> form_validation -> set_rules('account_type', 'Account Type', 'trim|required');

		if ($this -> form_validation -> run() == FALSE) {
			$this -> load -> view('home');
		} else {
			//assign
			$email = $this -> input -> post('email');
			$title_signup = $this -> input -> post('title_signup');
			$full_name = $this -> input -> post('full_name');
			$password = $this -> input -> post('password');
			$account_type = $this -> input -> post('account_type');

			//Save to database
			$this -> signup_model -> singupUser($email, $title_signup, $full_name, sha1($password), $account_type);
			
			//redirect
			if($account_type=='student')
			{
				redirect('editprofile_student');	
			}
			else {
				redirect('editprofile_faculty');
			}
		}
	}

	function check_database_for_email($email) {

		//check if email already exist
		$result = $this -> signup_model -> check_email($email);
		if ($result) {
			$this -> form_validation -> set_message('check_database_for_email', 'Email alreay registered!');
			return false;
		} else {
			return TRUE;
		}
	}

}
?>