<?php

class viewmybooks_faculty extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		$this -> load -> model('faculty_edit_books');
		$this -> load -> helper('my_helper');

		if ($this -> session -> userdata('logged_in')) {
			$session_data = $this -> session -> userdata('logged_in');

			$data['user_email'] = $session_data['user_email'];
			$data['user_full_name'] = $session_data['user_full_name'];

			$data['faculty_all_books'] = $this -> faculty_edit_books -> getFacultyBooks($session_data['user_email']);

			$this -> form_validation -> set_rules('book_title', 'Book Title', 'trim|required');
			$this -> form_validation -> set_rules('book_isbn', 'Book ISBN', 'trim|required');
			$this -> form_validation -> set_rules('book_description', 'Book Description', 'trim');
			$this -> form_validation -> set_rules('book_publication_year', 'Publication Year', 'trim|required');
			$this -> form_validation -> set_rules('book_subject_code', 'Subject Code', 'trim');

			if ($this -> form_validation -> run() == FALSE) {
				$this -> load -> view('viewmybooks_faculty_view', $data);
			} else {
				//assign
				$email = $session_data['user_email'];
				$book_title = $this -> input -> post('book_title');
				$book_isbn = $this -> input -> post('book_isbn');
				$book_description = $this -> input -> post('book_description');
				$book_publication_year = $this -> input -> post('book_publication_year');
				$book_subject_code = $this -> input -> post('book_subject_code');

				//Save to database
				if (isset($_POST)) {
					$this -> faculty_edit_books -> insertNewBook($email, $book_title, $book_isbn, $book_description, $book_publication_year, $book_subject_code);
					$_POST = null;
				}
				//redirect
				redirect('viewmybooks_faculty');
			}

		} else {
			//If no session, redirect to login page
			redirect('home', 'refresh');
		}

	}

}
?>