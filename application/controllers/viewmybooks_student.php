<?php

class viewmybooks_student extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		$this -> load -> model('student_books');

		if ($this -> session -> userdata('logged_in')) {
			$session_data = $this -> session -> userdata('logged_in');

			$data['user_email'] = $session_data['user_email'];
			$data['user_full_name'] = $session_data['user_full_name'];
			
			$data['student_all_books'] = $this -> student_books -> getStudentBooks($session_data['user_email']);	
			
			$this -> load -> view('viewmybooks_student_view', $data);

		} else {
			//If no session, redirect to login page
			redirect('home', 'refresh');
		}

	}

}
?>