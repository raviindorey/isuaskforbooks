<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

function timestamp_to_date($timestamp){
   return date('Y/m/d', $timestamp);
}

function date_to_timestamp($date){
   return strtotime($date);
}

function get_data_from_book_reservation($book_id) {
	$CI = get_instance();
	$CI -> load -> model('reserve_book');
	$query = $CI -> reserve_book -> getAllBooks_from_book_reservation($book_id);
	return $query;
}
?>