<?php

class Faculty_edit_books extends CI_Model {

	function __construct() {

		parent::__construct();

	}

	function insertNewBook($email, $book_title, $book_isbn, $book_description, $book_publication_year, $book_subject_code) {
		//Insert Query
		$query = $this -> db -> query("INSERT INTO `faculty_books`(`faculty_email`, `book_title`, `book_description`, `book_isbn`, `book_subject_code`, `book_publication_year`) VALUES  
		('" . $email . "',
		'" . $book_title . "',
		'" . $book_description . "',
		'" . $book_isbn . "',
		'" . $book_subject_code . "',
		'" . $book_publication_year . "')");

	}

	function getFacultyBooks($email) {
		$query = $this -> db -> get_where('faculty_books', array('faculty_email =' => $email));
		return $query;
	}

	function getBookReservation($book_id) {
		$query = $this -> db -> get_where('book_reservation', array('book_id =' => $book_id));
		return $query;
	}

}
?>