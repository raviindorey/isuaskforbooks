<?php

class reserve_book extends CI_Model {

	function __construct() {

		parent::__construct();

	}

	function insertHistory($request_id, $student_email, $user_email, $start_date, $return_date, $book_title) {
		//Insert Query
		$query = $this -> db -> query("INSERT INTO `book_history`(`reserve_id`,`reserve_to`, `reserve_from`, `reserve_date_start`, `reserve_date_end`,`book_title`) VALUES  
		('" . $request_id . "',
		'" . $student_email . "',
		'" . $user_email . "',
		'" . $start_date . "',
		'" . $return_date . "',
		'" . $book_title . "')");

	}

	function getBooks($email) {
		$this -> db -> order_by('book_subject_code', 'asc');
		$query = $this -> db -> get_where('book_history', array('reserve_to =' => $email));
		return $query;
	}

	function getAllBooks_from_faculty_books() {
		$this -> db -> order_by('book_subject_code', 'asc');
		$query = $this -> db -> get('faculty_books');
		return $query;
	}

	function getAllBooks_from_book_reservation($book_id) {
		$query = $this -> db -> get_where('book_reservation', array('book_id =' => $book_id));
		return $query;
	}

	function getFaculty_from_faculty_books($book_id) {
		$query = $this -> db -> get_where('faculty_books', array('book_id =' => $book_id));
		return $query;
	}

	function getFacultyInfo_from_faculty_information($faculty_email) {
		$query = $this -> db -> get_where('faculty_information', array('faculty_email =' => $faculty_email));
		return $query;
	}

	function getName_from_user_table($email) {
		$query = $this -> db -> get_where('user_table', array('user_email =' => $email));
		return $query;
	}

	function getBookReservation_from_book_reservation($book_id) {
		$query = $this -> db -> get_where('book_reservation', array('book_id =' => $book_id));
		return $query;
	}

	function setBookRequest($book_id, $student_email, $faculty_email, $start_date, $return_date) {
		$query = $this -> db -> query("INSERT INTO `book_request`(`book_id`, `student_email`, `faculty_email`, `start_date`, `return_date`) VALUES 
		('" . $book_id . "',
		'" . $student_email . "',
		'" . $faculty_email . "',
		'" . $start_date . "',
		'" . $return_date . "')");
	}

	function getAllRequests($user_email, $account_type) {
		if ($account_type == '1') {
			$query = $this -> db -> get_where('book_request', array('faculty_email =' => $user_email));
			return $query;
		} else {
			$query = $this -> db -> get_where('book_request', array('student_email =' => $user_email));
			return $query;
		}

	}

	function getRequestDetail($request_id) {
		$query = $this -> db -> get_where('book_request', array('request_id =' => $request_id));
		return $query;
	}

	function approveBookrequest($book_id, $book_title, $start_date, $return_date, $student_email) {
		$query = $this -> db -> get_where('book_reservation', array('book_id =' => $book_id));
		if ($query -> num_rows() > 0) {
			$this -> db -> query("UPDATE `book_reservation` SET 
				`book_title`='".$book_title."',
				`reserve_start`='".$start_date."',
				`reserve_end`='".$return_date."',
				`given_to`='".$student_email."'
				WHERE book_id = '".$book_id."'" );
		} else {
			$query = $this -> db -> query("INSERT INTO `book_reservation`(`book_id`, `book_title`, `reserve_start`, `reserve_end`, `given_to`) VALUES 
		('" . $book_id . "',
		'" . $book_title . "',
		'" . $start_date . "',
		'" . $return_date . "',
		'" . $student_email . "')");
		}
	}
	
	function deleteApprovedRequest($request_id){
		$this->db->delete('book_request', array('request_id =' => $request_id)); 
	}
	
	function getSearchBooks_from_faculty_books($search_keyword){
		$query = $this -> db -> get_where('faculty_books', array('faculty_email =' => $search_keyword));
			return $query;
			
	}

}
?>