<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Latest compiled and minified CSS -->
		<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

		<!--Local Bootstrap CSS-->
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/bootstrap.css">
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/readablebootstrap.min.css">

		<!-- jQuery library -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->

		<!---Local jQuery-->
		<script type="text/javascript" src="/isuaskforbooks/resource/js/jquery-2.1.4.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

		<!--Local Bootstrap Javascript-->
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.js"></script>
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

		<title>Ask For Books</title>

	</head>

	<body background="/isuaskforbooks/resource/images/ISU_Bridge_Faded.jpg">
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo site_url('find_books')?>">Ask For Books</a>
				</div>

				<div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('find_books')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Find Books" title="find Books" id="find_books" name="find_books">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('viewmybooks_student')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="View My Books" title="View/Manage Books" id="viewmybooks_student" name="viewmybooks_student">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('book_requests')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Book Requests" title="Book Requests" id="book_requests" name="book_requests">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('editprofile_student')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default active" value="<?php echo $user_full_name ?>" title="View/Edit Profile" id="editprofile_student" name="editprofile_student">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('logout')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Logout" title="Logout" id="Logout" name="Logout">
								</div>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div><!--END NAV-->

		<div class="container-fluid">
			<div class="row" id="signup_area" style="padding: 80px">
				<div class="col-md-1"></div><!--END COL-MD1-->

				<div class="col-md-10">
					<div class="text-center">
						<form role="form" id="ediprofile_student" action="editprofile_student" method="post" accept-charset="utf-8">
							<h3>Edit Profile</h3>
							<div class="row">
								<div class="col-md-2">
									<div class="form-group">
										<label for="about">About</label>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<textarea class="form-control" rows="10" id="about" name="about"><?php echo $student_about?></textarea>										
										<?php echo form_error('about'); ?>

									</div>
								</div>
							</div><!--end about-->
							<div class="row">
								<div class="col-md-2">
									<div class="form-group">
										<label for="subjects">Subjects:</label>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<input type="text" class="form-control" id="subjects" name="subjects" value="<?php echo $student_subjects?>" placeholder="Eg: IT-400 , IT-..">
										</input>
										<?php echo form_error('subjects'); ?>
									</div>
								</div>
							</div><!--end subjects-->
							<div class="row">
								<div class="col-md-2">
									<div class="form-group">
										<label for="contact_email1">Email 1:</label>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<input type="text" class="form-control" id="contact_email1" name="contact_email1" value="<?php echo $user_email?>" disabled>
										</input>
									</div>
								</div>
							</div><!--end contact email 1-->
							<div class="row">
								<div class="col-md-2">
									<div class="form-group">
										<label for="contact_email2">Email 2:</label>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<input type="text" class="form-control disabled" id="contact_email2" name="contact_email2" value="<?php echo $student_contact_email2?>" placeholder="Conatact Email 2">
										</input>
										<?php echo form_error('contact_email2'); ?>
									</div>
								</div>
							</div><!--end contact email 2-->
							<div class="row">
								<div class="col-md-2">
									<div class="form-group">
										<label for="contact_phone1">Phone:</label>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<input type="text" class="form-control disabled" id="contact_phone1" name="contact_phone1" value="<?php echo $student_contact_phone1?>" placeholder="Conatact Phone">
										</input>
										<?php echo form_error('contact_phone1'); ?>
									</div>
								</div>
							</div><!--end contact email 1-->
							<div class="row">
								<div class="col-md-2">
									<div class="form-group">
										<input type="submit" class="btn btn-primary" value="<?php echo $button_value?>">
										</input>
									</div>
								</div>
							</div><!--End Button-->
						</form>
					</div>
				</div><!--end col-md-10-->
			</div><!--end row-->
		</div><!--end container-->
	</body>
</html>