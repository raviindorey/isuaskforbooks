<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Latest compiled and minified CSS -->
		<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

		<!--Local Bootstrap CSS-->
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/bootstrap.css">
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/readablebootstrap.min.css">

		<!-- jQuery library -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->

		<!---Local jQuery-->
		<script type="text/javascript" src="/isuaskforbooks/resource/js/jquery-2.1.4.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

		<!--Local Bootstrap Javascript-->
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.js"></script>
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

		<title>Ask For Books</title>

	</head>

	<body background="/isuaskforbooks/resource/images/ISU_Bridge_Faded.jpg">
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo site_url($viewmybooks)?>">Ask For Books</a>
				</div>

				<div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('find_books')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default active" value="Find Books" title="find Books" id="find_books" name="find_books">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo $viewmybooks?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="View My Books" title="View/Manage Books" id="viewmybooks" name="viewmybooks">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('book_requests')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Book Requests" title="Book Requests" id="book_requests" name="book_requests">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo $editprofile?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="<?php echo $user_full_name ?>" title="View/Edit Profile" id="editprofile_faculty" name="editprofile_faculty">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('logout')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Logout" title="Logout" id="Logout" name="Logout">
								</div>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div><!--END NAV-->

		<div class="container-fluid">
			<div class="row" id="viewmybooks_faculty" style="padding: 80px">
				<div class="col-md-1"></div><!--END COL-3-->

				<div class="col-md-10">
					<form action="<?php echo site_url('search')?>" accept-charset="utf-8" method="post" >
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Search Faculty by Email" id="search_text" name="search_text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<button class="btn btn-primary" type="submit">
											Search
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>

					<div class="text-center">
						<h3>All Books</h3>
						<div class="row" id="viewbooks_table">
							<table class="table table-hover">
								<tr>
									<th>Book Title</th>
									<th>Book ISBN</th>
									<th>Subjects</th>
									<th>Pub. Year</th>
									<th>Contact Faculty</th>
								</tr>
								<?php
								if ($faculty_all_books -> num_rows() > 0) {
									foreach ($faculty_all_books->result() as $row) {
										echo "<tr>";

										echo "<td>";
										echo $row -> book_title;
										echo "</td>";
										echo "<td>";
										echo $row -> book_isbn;
										echo "</td>";
										echo "<td>";
										echo $row -> book_subject_code;
										echo "</td>";
										echo "<td>";
										echo $row -> book_publication_year;
										echo "</td>";
										echo "<td><a class='btn btn-primary' value='Request Book' href=" . site_url('find_books/redirectToRequest') . "/" . $row -> book_id . ">Request Book</a></td>";
										echo "</tr>";
									}
								} else {
									echo '<tr><td colspan="5"><strong>No Books Available For Now</strong></td></tr>';
								}
								?>
							</table>
							<!--INCLUDE DELETE BUTTON-->
						</div><!--END BOOK TABLE VIEW-->
					</div>
				</div><!--end COL-MD10-->

				<div class="col-md-1"></div><!--END COL-md1-->
			</div><!--end features row-->
		</div><!--end container-->

	</body>
</html>