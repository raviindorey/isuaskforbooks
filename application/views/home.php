<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//Created By Madhuri Choudhary.
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Latest compiled and minified CSS -->
		<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

		<!--Local Bootstrap CSS-->
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/bootstrap.css">
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/readablebootstrap.min.css">

		<!-- jQuery library -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->

		<!---Local jQuery-->
		<script type="text/javascript" src="/isuaskforbooks/resource/js/jquery-2.1.4.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

		<!--Local Bootstrap Javascript-->
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.js"></script>
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

		<title>Ask For Books</title>

	</head>

	<body background="/isuaskforbooks/resource/images/ISU_Bridge_Faded.jpg">
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home">Ask For Books</a>
				</div>

				<div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">

					<form class="navbar-form navbar-right form-inline" id="login" action="login" accept-charset="utf-8" method="post">
						<div class="form-group">
							<input class="form-control has-error" id="email_login" name="email_login" placeholder="Email" type="email" value="" tabindex="1" autofocus="autofocus">
						</div>
						<div class="form-group">
							<input class="form-control has-error" id="password_login" name="password_login" placeholder="Password" type="password" tabindex="2">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary" tabindex="3">
								Login
							</button>
						</div>
					</form>
				</div>
			</div>
			<div class="row pull-right">
				<p id="login_error_email"><font color="#b94a48" size="1"><?php echo form_error('email_login'); ?></font></p>
				<p id="login_error_password"><font color="#b94a48" size="1"><?php echo form_error('password_login'); ?></font></p>
			</div>
		</div><!--END NAV-->

		<div class="container-fluid">
			<div class="row" id="signup_area" style="padding: 80px">
				<div class="col-md-3"></div><!--END COL-3-->

				<div class="col-md-6">
					<div class="text-center">
						<form role="form" id="signup" action="signup" method="post" accept-charset="utf-8">
							<h3>Sign Up</h3>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<input type="text" name="title_signup" id="title_signup" class="form-control input-lg" placeholder="Title" value="<?php echo set_value('title'); ?>" tabindex="4">
										<p><font color="#b94a48" size="1"><?php echo form_error('first_name'); ?></font></p>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<input type="text" name="full_name" id="full_name" class="form-control input-lg" placeholder="Full Name" value="<?php echo set_value('full_name'); ?>" tabindex="5">
										<p><font color="#b94a48" size="1"><?php echo form_error('middle_name'); ?></font></p>
									</div>
								</div>
							</div>
							<div class="form-group">
								<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address" value="<?php echo set_value('email'); ?>" tabindex="7">
								<p><font color="#b94a48" size="1"><?php echo form_error('email'); ?></font></p>
							</div>
							<div class="form-group">
								<input type="email" name="email_confirm" id="email_confirm" class="form-control input-lg" placeholder="Confirm Email Address" value="<?php echo set_value('email'); ?>" tabindex="7">
								<p><font color="#b94a48" size="1"><?php echo form_error('email_confirm'); ?></font></p>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="8">
										<p><font color="#b94a48" size="1"><?php echo form_error('password'); ?></font></p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password" tabindex="9">
										<p><font color="#b94a48" size="1"><?php echo form_error('password_confirmation'); ?></font></p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="well">
									<div class="form-group">
										<div class="col-md-6">
											You are a:
										</div>
										<div class="col-md-6">
											<label class="radio-inline">
												<input type="radio" name="account_type" id="inlineRadio1" value="student">
												Student </label>
											<label class="radio-inline">
												<input type="radio" name="account_type" id="inlineRadio2" value="faculty">
												Faculty </label>
										</div>
										<?php echo form_error('account_type'); ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<input type="submit" value="Register" class="btn btn-primary btn-block btn-lg" tabindex="9">
								</div>
							</div>
						</form>
					</div><!--End panel-->
				</div><!--end COL-LG6-->
				<div class="col-md-3"></div><!--END COL-3-->
			</div><!--end features row-->
		</div><!--end container-->

	</body>
</html>