<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Latest compiled and minified CSS -->
		<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

		<!--Local Bootstrap CSS-->
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/bootstrap.css">
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/readablebootstrap.min.css">

		<!-- jQuery library -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
		
		<!---Local jQuery-->
		<script type="text/javascript" src="/isuaskforbooks/resource/js/jquery-2.1.4.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

		<!--Local Bootstrap Javascript-->
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.js"></script>
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>


		<title>Ask For Books</title>

	</head>

	<body background="/isuaskforbooks/resource/images/ISU_Bridge_Faded.jpg">
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo site_url('find_books')?>">Ask For Books</a>
				</div>

				<div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('find_books')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Find Books" title="find Books" id="find_books" name="find_books">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('viewmybooks_student')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default active" value="View My Books" title="View/Manage Books" id="viewmybooks_student" name="viewmybooks_student">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('book_requests')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Book Requests" title="Book Requests" id="book_requests_student" name="book_requests_student">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('editprofile_student')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="<?php echo $user_full_name ?>" title="View/Edit Profile" id="editprofile_student" name="editprofile_student">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('logout')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Logout" title="Logout" id="Logout" name="Logout">
								</div>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div><!--END NAV-->

		<div class="container-fluid">
			<div class="row" id="viewmybooks_faculty" style="padding: 80px">
				<div class="col-md-1">
				</div><!--END COL-3-->

				<div class="col-md-10">
					<div class="text-center">
						
						<h3>Request Book</h3>
						<br>
						<h4>Faculty Information</h4>
							<div class="row" id="viewbooks_table">
								<table class="table">
									<tr>
										<td>Faculty Name</td>
										<td><?php echo $faculty_full_name?></td>
									</tr>
									<tr>
										<td>About Faculty</td>
										<td><?php echo $faculty_about?></td>
									</tr>
									<tr>
										<td>Subjects Under</td>
										<td><?php echo $faculty_classes?></td>
									</tr>
									<tr>
										<td>Office Hours</td>
										<td><?php echo $faculty_office_hours?></td>
									</tr>
									<tr>
										<td>Office Location</td>
										<td><?php echo $faculty_office_location?></td>
									</tr>
									<tr>
										<td>Contact Email</td>
										<td><a href="<?php echo "mailto:".$faculty_email?>"><?php echo $faculty_email?></a></td>
									</tr>
									<tr>
										<td>Other Email</td>
										<td><a href="<?php echo "mailto:".$faculty_email_2?>"><?php echo $faculty_email_2?></a></td>
									</tr>
									<tr>
										<td>Phone Number</td>
										<td><?php echo $faculty_phone_1?></td>
									</tr>

								</table>
								<!--INCLUDE DELETE BUTTON-->
							</div><!--END BOOK TABLE VIEW-->
							<div class="row">
								<table class="table">
									<tr>
										<td><strong>Book Title</strong></td>
										<td><?php echo $book_title?></td>
									</tr>
									<tr>
										<td><strong>Book ID</strong></td>
										<td><?php echo $book_id?></td>
									</tr>
									<tr>
										<td><strong>Next Available</strong></td>
										<td><?php echo $next_available?></td>
									</tr>
									<tr>
										<td><strong>Reserve this book:</strong></td>
										<td>
											<form role="form" action="<?php echo site_url('ask_books')?>" method="post" accept-charset="utf-8">
												<div class="form-group">
													<input class="form-control" type="text" id="req_datetime" name="req_datetime_from" placeholder="From (YYYY/MM/DD)"/>
												</div>
												<div class="form-group">
													<input class="form-control" type="text" id="req_datetime" name="req_datetime_till" placeholder="Till (YYYY/MM/DD)"/>
												</div>
												<div class="form-group">
													<button class="btn btn-primary" type="submit">Request Book</button>
												</div>
											</form>
										</td>
									</tr>
								</table>
							</div>
						</div><!--END COL MD10-->
				</div><!--end COL-LG6-->
				<div class="col-md-1">
				</div><!--END COL-3-->
			</div><!--end features row-->
		</div><!--end container-->
	</body>
	</html>