<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Latest compiled and minified CSS -->
		<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

		<!--Local Bootstrap CSS-->
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/bootstrap.css">
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/readablebootstrap.min.css">

		<!-- jQuery library -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
		
		<!---Local jQuery-->
		<script type="text/javascript" src="/isuaskforbooks/resource/js/jquery-2.1.4.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

		<!--Local Bootstrap Javascript-->
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.js"></script>
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>


		<title>Ask For Books</title>

	</head>

	<body background="/isuaskforbooks/resource/images/ISU_Bridge_Faded.jpg">
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo site_url('viewmybooks_faculty')?>">Ask For Books</a>
				</div>

				<div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('find_books')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Find Books" title="find Books" id="find_books" name="find_books">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('viewmybooks_faculty')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default active" value="View My Books" title="View/Manage Books" id="viewmybooks_faculty" name="viewmybooks_faculty">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('book_requests')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Book Requests" title="Book Requests" id="book_requests" name="book_requests">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('editprofile_faculty')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="<?php echo $user_full_name ?>" title="View/Edit Profile" id="editprofile_faculty" name="editprofile_faculty">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('logout')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Logout" title="Logout" id="Logout" name="Logout">
								</div>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div><!--END NAV-->

		<div class="container-fluid">
			<div class="row" id="viewmybooks_faculty" style="padding: 80px">
				<div class="col-md-1">
				</div><!--END COL-3-->

				<div class="col-md-10">
					<div class="text-center">
						
						<h3>My Books</h3>
							<div class="row" id="viewbooks_table">
								<table class="table table-hover">
									<tr>
										<td><strong>Book Title</strong></td>
										<td><strong>Book ISBN</strong></td>
										<td><strong>Subjects</strong></td>
										<td><strong>Pub. Year</strong></td>
										<td><strong>Reservation</strong></td>
										
									</tr>
									<?php
									foreach ($faculty_all_books->result() as $row) {
										echo "<tr>";

										echo "<td>";
										echo $row -> book_title;
										echo "</td>";
										echo "<td>";
										echo $row -> book_isbn;
										echo "</td>";
										echo "<td>";
										echo $row -> book_subject_code;
										echo "</td>";
										echo "<td>";
										echo $row -> book_publication_year;
										echo "</td>";

										$query = $this -> db -> get_where('book_reservation', array('book_id =' => $row -> book_id));
										if($query->num_rows()>0){
											foreach ($query->result() as $row1) {
											echo '<td class="danger">';
												echo $row1 -> given_to . "<br>".timestamp_to_date($row1 -> reserve_end);
											echo "</td>";
										}
										
										
										}else{
											echo'<td class="success">Book Not Reserved</td>';
										}
										echo "<td><a class='btn btn-primary' value='Delete' href=" . site_url('faculty_managebooks_controller/delete_book') . "/" . $row -> book_id . ">Delete Book</a></td>";
										echo "</tr>";
									}
								?>
								</table>
								<!--INCLUDE DELETE BUTTON-->
							</div><!--END BOOK TABLE VIEW-->
						<br>
						<h3>Add new book</h3>
						<form role="form" id="addbook_faculty" action="viewmybooks_faculty" method="post" accept-charset="utf-8">	
							<div class="container" id="addbooks">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input type="text" name="book_title" id="boot_title" class="form-control input-lg" placeholder="Book Title">
											<p><font color="#b94a48" size="1"><?php echo form_error('book_title'); ?></font></p>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input type="text" name="book_isbn" id="book_isbn" class="form-control input-lg" placeholder="ISBN">
											<p><font color="#b94a48" size="1"><?php echo form_error('book_isbn'); ?></font></p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<textarea class="form-control" rows="6" name="book_description" id="book_description" placeholder="Book Description"></textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input type="text" name="book_publication_year" id="book_publication_year" class="form-control input-lg" placeholder="Publication Year(integer)">
											<p><font color="#b94a48" size="1"><?php echo form_error('book_publication_year'); ?></font></p>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input type="text" name="book_subject_code" id="book_subject_code" class="form-control input-lg" placeholder="Relevant Subject Codes">
											<p><font color="#b94a48" size="1"><?php echo form_error('book_subject_code'); ?></font></p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<input type="submit" value="Add Book" class="btn btn-primary">
									</div>
								</div>
							</div><!--END ADDD BOOKS-->
						</form>
					</div><!--End panel-->
				</div><!--end COL-LG6-->
				<div class="col-md-1"></div><!--END COL-3-->
			</div><!--end features row-->
		</div><!--end container-->
		
	</body>
	</html>