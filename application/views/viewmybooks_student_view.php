<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Latest compiled and minified CSS -->
		<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

		<!--Local Bootstrap CSS-->
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/bootstrap.css">
		<link rel="stylesheet" href="/isuaskforbooks/resource/bootstrap-3.3.5-dist/css/readablebootstrap.min.css">

		<!-- jQuery library -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
		
		<!---Local jQuery-->
		<script type="text/javascript" src="/isuaskforbooks/resource/js/jquery-2.1.4.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

		<!--Local Bootstrap Javascript-->
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.js"></script>
		<script type="text/javascript" src="/isuaskforbooks/resource/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>


		<title>Ask For Books</title>

	</head>

	<body background="/isuaskforbooks/resource/images/ISU_Bridge_Faded.jpg">
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo site_url('viewmybooks_student')?>">Ask For Books</a>
				</div>

				<div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('find_books')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Find Books" title="find Books" id="find_books" name="find_books">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('viewmybooks_student')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default active" value="View My Books" title="View/Manage Books" id="viewmybooks_student" name="viewmybooks_student">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('book_requests')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Book Requests" title="Book Requests" id="book_requests_student" name="book_requests_student">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('editprofile_student')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="<?php echo $user_full_name ?>" title="View/Edit Profile" id="editprofile_student" name="editprofile_student">
								</div>
							</form>
						</li>
						<li>
							<form class="navbar-form form-inline" method="post" action="<?php echo site_url('logout')?>" accept-charset="utf-8">
								<div class="form-group">
									<input type="submit" class="btn btn-default" value="Logout" title="Logout" id="Logout" name="Logout">
								</div>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div><!--END NAV-->

		<div class="container-fluid">
			<div class="row" id="viewmybooks_faculty" style="padding: 80px">
				<div class="col-md-1">
				</div><!--END COL-3-->

				<div class="col-md-10">
					<div class="text-center">
						
						<h3>My Books</h3>
							<div class="row" id="viewbooks_table">
								<table class="table table-hover">
									<tr>
										<td><strong>Reserve ID</strong></td>
										<td><strong>Book Title</strong></td>
										<td><strong>Given By</strong></td>
										<td><strong>Issued From</strong></td>
										<td><strong>Issued Till</strong></td>
									</tr>
									<?php
										foreach ($student_all_books->result() as $row) 
										{
										echo "<tr>";
										echo "<td>"; echo $row -> reserve_id; echo "</td>";
										echo "<td>"; echo $row -> book_title; echo "</td>";
										echo "<td>"; echo $row -> reserve_from; echo "</td>";
										echo "<td>"; echo timestamp_to_date($row -> reserve_date_start); echo "</td>";
										echo "<td>"; echo timestamp_to_date($row -> reserve_date_end); echo "</td>";
										echo "</tr>";
										}?>
								</table>
								<!--INCLUDE DELETE BUTTON-->
							</div><!--END BOOK TABLE VIEW-->
						</div><!--END COL MD10-->
				</div><!--end COL-LG6-->
				<div class="col-md-1">
				</div><!--END COL-3-->
			</div><!--end features row-->
		</div><!--end container-->
	</body>
	</html>